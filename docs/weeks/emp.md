# Imbedded Programming


##Assigment

- Practicing with the Arduino Uno kit using all kindscomponents to make a circuit
- Installing Arduino-1.8.9 for windows
- Programming basics in the Arduino IDE


##Learning outcomes:

- Working with The Arduino Uno Board connecting it with our Laptop using a USB cable and giving the commands by codiing it in the Arduino IDE
- Knowing the Breadboard and all other components in the Arduino Kit and checking its specs online if needed by typing the Serial Number
-Understanding the Electrical Current Flow


##Assessment:

Have you:
- Build a basic circuit without using any codes
- Build various circuits using codes
- Learn Procedural Programming


1 This is the Arduino Board
![emp](../img/arboard.jpg)


Arduino is a small, inexpensive, programmable microcontroller that exposes a multitude of input and output (I/O) connections.

A jumper wire is used to connect components to each other on the breadboard.




2.
After installing Arduino Upload Blink Sketch Blink Built in Led 12 and 13
![emp](../img/Screenshotar.png)


This is what it looks like
![emp](../img/blinkon.jpg)


This is the code:

![emp](../img/codeblink.jpg)


3.
The code for the spaceship interphase is:

```
int switchstate = 0;
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(2, INPUT);
   
}


// the loop function runs over and over again forever
void loop() {
  switchstate = digitalRead (2);    // this is a command  
  if (switchstate ==LOW){
 
    digitalWrite(3, HIGH);   // turn the LED on (HIGH is the voltage level)
    digitalWrite(4, LOW);    // turn the LED off by making the voltage LOW
    digitalWrite(5, LOW);    // turn the LED off by making the voltage LOW
 
  }else{
 
    digitalWrite(3, LOW);   // turn the LED on (HIGH is the voltage level)
    digitalWrite(4, HIGH);    // turn the LED off by making the voltage LOW
    delay(250);                       // wait for a quater second
   
    //toggle the LEDS
    digitalWrite(4, LOW);    // turn the LED off by making the voltage LOW
    digitalWrite(5, HIGH);    // turn the LED off by making the voltage LOW
    delay(250);                       // wait for a quater second

    digitalWrite(3, HIGH);   // turn the LED on (HIGH is the voltage level)
    digitalWrite(4, LOW);    // turn the LED off by making the voltage LOW
    digitalWrite(5, LOW);    // turn the LED off by making the voltage LOW    
    delay(250);  
           
  }
 
}

```




4.
This same code you can tweak even further and optimize it (procedural programming).
With this code you get the running lights....


```
int switchstate = 0;

void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(2, INPUT);

}

void lightsout(){
      digitalWrite(3, LOW);    // turn the LED off by making the voltage LOW
      digitalWrite(4, LOW);    // turn the LED off by making the voltage LOW
      digitalWrite(5, LOW);    // turn the LED off by making the voltage LOW
 
}
void lightson(){
        lightsout();
        digitalWrite(3, HIGH);   // turn the LED on (HIGH is the voltage level)
        delay (100);
        lightsout();
        digitalWrite(4, HIGH);    // turn the LED off by making the voltage LOW
        delay (100);
        lightsout();
        digitalWrite(5, HIGH);    // turn the LED off by making the voltage LOW
        delay (100);  
 
  }
// the loop function runs over and over again forever
void loop() {
  switchstate = digitalRead (2);    // this is a command  
 
  if (switchstate ==LOW){
        lightsout();
         
  }else{
        lightson();
 
//    digitalWrite(3, LOW);   // turn the LED on (HIGH is the voltage level)



}
           
  }
```

This is the heroshot..
![emp](../img/chrismaslight.jpg)


 
 
5.
Serial Communication

The Serial communication is the communication between The Arduino Board and the IDE over the cable on the Com3 port. Using the Codes:

```
void setup() {
// setup your serial
Serial.begin(9600);
Serial.println("hello Rox");
}

void loop() {
// Send a message to your serial port/monitor
Serial.println(millis());
delay(2000);
}
```


LDR The Light Dependent Resistor, A variable resistor that changes its resistance based on the amount of light that falls on it. The value of the amount of light that falls on the LDR is not enough to reach the percentage.

The code is:

```
int lightInt = 0;
int lightPc = 0;
int minL = 0;
int maxL = 1023;

// the setup routine runs once when you press reset:
void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
  Serial.println ("helllo");
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
}

 void lightsout(){
  digitalWrite(3,0);    // turn the LED off by making the voltage LOW
  digitalWrite(4,0);    // turn the LED off by making the voltage LOW
  digitalWrite(5,0);    // turn the LED off by making the voltage LOW

  }

  void lightson(){
  lightsout();
  digitalWrite(3,1);   // turn the LED on (HIGH is the voltage level)
  delay (250);
  lightsout();
  digitalWrite(4,1);    // turn the LED off by making the voltage LOW
  delay (250);
  lightsout();
  digitalWrite(5,1);    // turn the LED off by making the voltage LOW
  delay (250);  

  }

// the loop routine runs over and over again forever:
void loop() {
  // read the input on analog pin 0:
  lightInt = analogRead(A0);
  lightPc = (lightInt - minL)*100L/(maxL-minL);
 // print out the value you read:
  Serial.println(lightPc);
  lightsout();

  // if light greater and equal to 30% led 1 on 
  // if light greater and equal to 60% led 2 on 
  // if light greater and equal to 90% led 3 on 
  if (lightPc>=30){digitalWrite(3,1);};
  if (lightPc>=60){digitalWrite(4,1);};
  if (lightPc>=90){digitalWrite(5,1);};

  if (lightPc>95){lightson();};

  delay(250);        // delay in between reads for stability
  }
```

 
 
  6.
  PWM, 
  The Arduino board is only capable of generating digital signals (HIGH and LOW), but analogWrite(); simulates the appearance of brightnesses between on and off using pulse width modulation (PWM). 
  The LED flashes on and off very quickly, and your eye interprets a dimmer light. 
  The ratio of time the LED spends on vs. off determines how bright or dim the LED appears. Only certain pins are capable of PWM, and they are labeled on the board with squiggles ~ next to the pin number.
 

The code for PWM:
  
 
```
 // the setup routine runs once when you press reset:
void setup() {

  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
}

 void dimLed(int ledPin,int dutyCycle){
   //cycletime =100ms
   //led Aan
   digitalWrite (ledPin,1);
   delay (dutyCycle);

   //Led Uit
   digitalWrite (ledPin,0);
   delay (100 - dutyCycle);

  }

// the loop routine runs over and over again forever:
void loop() {
  dimLed(5,75);

  }
PWM 1.2 Working with cycletime and dimLed

int cycleTime = 10;   //cycletime  inms
```


```
// the setup routine runs once when you press reset:
void setup() {

  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
}

 void dimLed(int ledPin,int dutyCycle){


   //led Aan
   digitalWrite (ledPin,1);
   delay (dutyCycle*cycleTime/100);

   //Led Uit
   digitalWrite (ledPin,0);
   delay (cycleTime - (dutyCycle*cycleTime/100));

  }

// the loop routine runs over and over again forever:
void loop() {
  dimLed(5,75);

  }
```
  

 with 2 LED's the code is:
 

 
```
 int cycleTime = 10;            //cycletime  in ms
  int analogCycleTime = 0.50*255;      // Analog cycletime goes from 0 -255

// the setup routine runs once when you press reset:
void setup() {

  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
}

 void dimLed(int ledPin,int dutyCycle){


   //led Aan
   digitalWrite (ledPin,1);
   delay (dutyCycle*cycleTime/100);

   //Led Uit
   digitalWrite (ledPin,0);
   delay (cycleTime - (dutyCycle*cycleTime/100));

  }

  // the loop routine runs over and over again forever:
void loop() {
  dimLed(5,10);

  analogWrite(3,analogCycleTime);

  }
```

 
Arduino Uno Pin Diagram PWM - pin on 3, 5, 6, 9, 11 and Provides 8-bit PWM output.



7.
The DHT-11 is a digital-output relative humidity and temperature sensor. It uses a capacitive humidity sensor and a thermistor to measure the surrounding air. 
It is integrated  with a high-performance 8-bit microcontroller. Its technology ensures the high reliability and excellent long-term stability.  
This sensor includes a resistive element and a sensor for wet NTC temperature measuring devices.
The DHT11 uses just one signal wire to transmit data to the Arduino. Power comes from separate 5V and ground wires. 
A 10K Ohm pull-up resistor is needed between the signal line and 5V line to make sure the signal level stays high by default .


The DHT Example Code:




```
void setup() {
  Serial.begin(9600);
  Serial.println(F("DHTxx test!"));

  dht.begin();
}

void loop() {
  // Wait a few seconds between measurements.
  delay(2000);

  // Reading temperature or humidity takes about 250 milliseconds!
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  float h = dht.readHumidity();
  // Read temperature as Celsius (the default)
  float t = dht.readTemperature();
  // Read temperature as Fahrenheit (isFahrenheit = true)
  float f = dht.readTemperature(true);

  // Check if any reads failed and exit early (to try again).
  if (isnan(h) || isnan(t) || isnan(f)) {
    Serial.println(F("Failed to read from DHT sensor!"));
    return;
  }

  // Compute heat index in Fahrenheit (the default)
  float hif = dht.computeHeatIndex(f, h);
  // Compute heat index in Celsius (isFahreheit = false)
  float hic = dht.computeHeatIndex(t, h, false);

  Serial.print(F("Humidity: "));
  Serial.print(h);
  Serial.print(F("%  Temperature: "));
  Serial.print(t);
  Serial.print(F("°C "));
  Serial.print(f);
  Serial.print(F("°F  Heat index: "));
  Serial.print(hic);
  Serial.print(F("°C "));
  Serial.print(hif);
  Serial.println(F("°F"));
}
```




A DC motor (Direct Current motor) is the most common type of motor. 
DC motors normally have just two leads, one positive and one negative. 
If you connect these two leads directly to a battery, the motor will rotate. If you switch the leads, the motor will rotate in the opposite direction.



The code is:


```
void setup()
{
  pinMode(3, OUTPUT);
}

void loop()
{
  digitalWrite(3, HIGH);
  delay(1000); // Wait for 1000 millisecond(s)
  digitalWrite(13, LOW);
  delay(1000); // Wait for 1000 millisecond(s)
}
```

![emp](../img/DC2.jpg)




8.
H-Bridge
A more versatile way of controlling a DC motor is to use a circuit called an “H-Bridge”. 
An “H-Bridge” is an arrangement of transistors that allow you to control both the direction and speed of the motor. 
An “H-Bridge” is simply an arrangement of switching the polarity of the voltage applied to a DC motor, thus controlling its direction of rotation. 
Using transistors allows you to control the motor speed with PWM.
Unfortunately, servos do not turn 360 degrees but only 180.  DC Motors does have the capability to turn 360 degrees around.
If you were to take power and ground on the motor and flip their orientation, the motor would spin in the opposite direction. 
However, it is not very convenient to do that every time you want to spin the DC Motor in another direction. 
So, we used an H-Bridge to reverse the polarity of the motor.

code with speed control:



```

const int pwm = 3 ; //initializing pin 2 as pwm
const int in_1 = 8 ;
const int in_2 = 9 ;
// Speed control
Const int speedPin = A0;
Int speed =0;

//For providing logic to L298 IC to choose the direction of the DC motor

void setup() {
   pinMode(pwm,OUTPUT) ; //we have to set PWM pin as output
   pinMode(in_1,OUTPUT) ; //Logic pins are also set as output
   pinMode(in_2,OUTPUT) ;
}

void loop() {
   // Detect speedPin value
   speed=analogRead(speedPin)/4;
   //For Clock wise motion , in_1 = High , in_2 = Low
   digitalWrite(in_1,HIGH) ;
   digitalWrite(in_2,LOW) ;
   analogWrite(pwm,speed) ;
   /* setting pwm of the motor to 255 we can change the speed of rotation
   by changing pwm input but we are only using arduino so we are using highest
   value to driver the motor */
   //Clockwise for 3 secs
   delay(3000) ;
   //For brake
   digitalWrite(in_1,HIGH) ;
   digitalWrite(in_2,HIGH) ;
   delay(1000) ;
   //For Anti Clock-wise motion - IN_1 = LOW , IN_2 = HIGH
   digitalWrite(in_1,LOW) ;
   digitalWrite(in_2,HIGH) ;
   delay(3000) ;
   //For brake
   digitalWrite(in_1,HIGH) ;
   digitalWrite(in_2,HIGH) ;
   delay(1000) ;
}

```

![ep](../img/hbridge.png)


9. 
Motor control Servo
A servo motor is a closed-loop system that uses position feedback to control its motion and final position.
Inside hobby servo there are four main components, a DC motor, a gearbox, a potentiometer and a control circuit. 
The potentiometer is attached on the final gear or the output shaft, so as the motor rotates the potentiometer rotates as well, thus producing a voltage that is related to the absolute angle of the output shaft. 
In the control circuit, this potentiometer voltage is compared to the voltage coming from the signal line. 
If needed, the controller activates an integrated H-Bridge which enables the motor to rotate in either direction until the two signals reach a difference of zero.

A servo motor is controlled by sending a series of pulses through the signal line. 
The frequency of the control signal should be 50Hz or a pulse should occur every 20ms. The width of pulse determines angular position of the servo and these type of servos can usually rotate 180 degrees


the code is:


```
#include <Servo.h>
Servo myServo;
int const potPin = A0;
int potVal;
int angle;
void setup(){
  myServo.attach(9);
  Serial.begin(9600);
}
void loop(){
  potVal=analogRead(potPin);
  Serial.print("potVal:");
  Serial.print (potVal);
  angle=map(potVal,0,1023,0,179);
  Serial.print(",angle:");
  Serial.println(angle);
  //analogWrite(9, map(potVal,0,1023,0,255));
  myServo.write(angle);
  delay(15);
}
```


![ep](../img/servomotor.jpg)

 









