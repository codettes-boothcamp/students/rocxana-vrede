The Raspberry Pi is a very cheap computer that runs Linux, but it also provides a set of GPIO (general purpose input/output) pins that allow you to control electronic components for physical computing and explore the Internet of Things (IoT).


 The tools that are needed for interface are: 
- Raspberry Pi 
- Ethernet or Cross Cables 
- Sd Card 16gb min 
- Power Cable micro usb 
- Laptop ethernet slot
 
 
 Basic Raspberry setup:
- Download Raspbian version (Jessie or Stretch)
- Install Disk32Imager
- Install Putty
- Install Winscp
- Write Raspbian 2 OS (Jessie on SD card using Win32DiskImager
- On SD card edit cmdline.txt (ip=192.168.1.xx see next slide)
- Create ssh file with no extension
- Configure pc ethernet adapter config to match rpi subnet but different address
- Insert SD card into Raspberry Pi
-  Connect Pi with pc using cross cable
-  Power up the Pi
- Ping (IP) -  Open Putty and enter IP-  Enter username and password (pi/*)
- Configure Raspbian.Run Rpi setup 
     wizard  type  > raspi-config
- Enable root user
- Install Nodejs & Npm
- Nodejs folder structure
- Install Python & PiP 
- Connect Winscp to Rpi

![iap](../img/interface1.png)


Network & Ethernet settings

After configuring the Ethernet settings with your ip-adress: 192.168.1.160 , plug your crosscable to your raspberryPi. 
Give it power by using your USB-cable. 
If it works, your plug where the ethernet cable is connected should give you lights-indicators. 



Login

Start your Putty.exe and login with your username and password.
Login: Pi 
Username: Raspberry



Nodejs

What is the use of Nodejs?
Node. js is a platform built on Chrome's JavaScript runtime for easily building fast and scalable network applications. 
Node. js uses an event-driven, non-blocking I/O model that makes it lightweight and efficient, 
perfect for data-intensive real-time applications that run across distributed devices.

Node.js is an open source, cross-platform runtime environment for developing server-side and networking applications. 
Node.js applications are written in JavaScript, and can be run within the Node.js runtime on OS X, Microsoft Windows, and Linux.

Node.js also provides a rich library of various JavaScript modules which simplifies the development of web applications using Node.js to a great extent.


JavaScript is a scripting language used to create and control dynamic website content, i.e. 
anything that moves, refreshes, or otherwise changes on your screen without requiring you to manually reload a web page. Features like:

- animated graphics
- photo slideshows
- autocomplete text suggestions
- interactive forms



