## Week 2 Video Productions


Task List for this week:
- Setup an Adobe Project
- Edit an Adobe Project

Screenshots:

- creating project file: Video and Audio preset
![vp](../img/vp1.jpg)


-importing files
![vp](../img/vp2.jpg)


-cutting and assembling the raw materials
![vp](../img/vp3.jpg)


-adding the ultra key and removing backgroung
![vp](../img/vpultra1.jpg)


-adding another image to background
![vp](../img/vp5.jpg)


-adding text to videos
![vp](../img/vp4.jpg)